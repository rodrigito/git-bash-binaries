# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

###########
# DIRENV  #
###########
# https://direnv.net/docs/installation.html

#eval "$(direnv hook bash)"


########################
# MAKE HUMAN FRIENDLY  #
########################

alias du="du -ach | sort -h"
alias free="free -mth"
alias ps="ps auxf"
alias dmesg='dmesg -wH'

# Make our process table searchable.
# We can create an alias that searches our process for an argument we'll pass
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
# so 'psg bash'

#######################
#   HISTORY RELATED   #
#######################

## Set the  maximum  number of lines contained in the history file
export HISTFILESIZE=50000

## Set the number of commands to remember in the command history
export HISTSIZE=15000

## Append it ##
shopt -s histappend

######
# Controlling how commands are saved on the history file
# ignoreboth means:
# a) Command which begin with a space character are not saved in the history list
# b) Command matching the previous history entry  to  not  be  saved (avoid duplicate commands)
######
#export HISTCONTROL=ignorespace
export HISTCONTROL=ignoreboth
